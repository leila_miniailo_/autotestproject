package com.harman.autotest;

import org.junit.After;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import static com.harman.autotest.SystemUtil.getDriverAccToSystemType;
import static org.hamcrest.core.Is.is;
import static org.hamcrest.core.StringStartsWith.startsWith;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertThat;

public class SeleniumTest {
  private static WebDriver driver;
  private static String baseUrl = "https://motobanda.pl";
  private static ChromeOptions options;
  private static String KASK_URL = baseUrl + "/ubrania/kask";

  @BeforeClass
  public static void setUp() {
    System.setProperty("webdriver.chrome.driver", getDriverAccToSystemType());
    options = new ChromeOptions();
    options.addArguments("--window-size=1366,768");
  }

  @Before
  public void startDriver() {
    driver = new ChromeDriver(options);
  }

  @Test
  public void shouldOpenKaskPageWhenUserClicksItOnStartPage() {
    String expectedUrl = KASK_URL;
    String expectedTitle = "Kaski";
    String cssPageHeader = ".card-header h1";
    String cssActiveInTitle = ".list-inline .active";
    String cssItemImage = ".image-with-description";

    driver.get(baseUrl + "/sklep");
    driver.findElements(By.cssSelector(cssItemImage)).get(0).click();

    assertThat("Opened page url is not as expected", driver.getCurrentUrl(), is(startsWith(expectedUrl)));
//    assertTrue("Page header is not Kaski", driver.findElement(By.cssSelector(cssPageHeader)).getText().equals(expectedTitle));
    assertEquals("Page header is not Kaski", expectedTitle, driver.findElement(By.cssSelector(cssPageHeader)).getText());
    assertEquals("Active element in list is not Kaski", expectedTitle.toUpperCase(), driver.findElement(By.cssSelector(cssActiveInTitle)).getText());
  }

  @Test
  public void shouldAddHelmetToCartWhenUserSearchedIt() throws InterruptedException {
    String cssSearchField = "#shop-search-input";
    String cssSearchButton = "#shop-search-btn";
    String cssFoundHelmet = ".row.cloth";
    String cssTitleFoundHelmet = ".title a";
    String cssPaginatorOnListPage = ".pagination";
    String buyButton = ".fs-large";
    String itemInCart = "div.name a";
    String totalCart = ".total";
    String cssCloseEmail = ".right-corner.close-btn";

    driver.get(KASK_URL);
    driver.findElement(By.cssSelector(cssSearchField)).sendKeys("Caberg Droid");
    driver.findElement(By.cssSelector(cssSearchButton)).sendKeys(Keys.ENTER);
    WebDriverWait wait = new WebDriverWait(driver, 10);
    wait.until(ExpectedConditions.invisibilityOf(driver.findElement(By.cssSelector(cssPaginatorOnListPage))));

    assertEquals("Not one helmet was found", 1, driver.findElements(By.cssSelector(cssFoundHelmet)).size());

    driver.findElement(By.cssSelector(cssCloseEmail)).click();
    Actions actions = new Actions(driver);
    actions.moveToElement(driver.findElement(By.cssSelector(cssTitleFoundHelmet))).perform();
    driver.findElement(By.cssSelector(cssTitleFoundHelmet)).click();
    wait.until(ExpectedConditions.titleContains("Caberg Droid"));
    driver.findElement(By.cssSelector(cssCloseEmail)).click();
    driver.findElements(By.cssSelector(buyButton)).get(0).click();
    driver.findElements(By.cssSelector(buyButton)).get(1).click();

    wait.until(ExpectedConditions.presenceOfElementLocated(By.cssSelector(itemInCart)));
    assertEquals("Price is not as expected", "949,00", driver.findElement(By.cssSelector(totalCart)).getText());
    assertEquals("Not one item is in cart", 1, driver.findElements(By.cssSelector(itemInCart)).size());
    assertEquals("Item in the cart is not as expected", "Kask Caberg Droid", driver.findElement(By.cssSelector(itemInCart)).getText());
  }

  @After
  public void closeDriver() {
    if (driver != null) {
      driver.close();
    }
  }
}
