package com.harman.autotest;


import com.codeborne.selenide.Condition;
import org.junit.BeforeClass;
import org.junit.Test;

import static com.codeborne.selenide.Condition.visible;
import static com.codeborne.selenide.Configuration.baseUrl;
import static com.codeborne.selenide.Selenide.*;
import static com.codeborne.selenide.WebDriverRunner.url;
import static junit.framework.TestCase.assertEquals;
import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.CoreMatchers.startsWith;
import static org.hamcrest.MatcherAssert.assertThat;

public class SelenideTest {

  @BeforeClass
  public static void setUp(){
    System.setProperty("selenide.browser", "Chrome");
    System.setProperty("selenide.baseUrl", "https://motobanda.pl");
    System.setProperty("selenide.browserSize", "1366x768");
  }

  @Test
  public void shouldOpenKaskPageWhenUserClicksItOnStartPage() {
    String expectedUrl = baseUrl + "/ubrania/kask";
    String expectedTitle = "Kaski";
    String cssPageHeader = ".card-header h1";
    String cssActiveInTitle = ".list-inline .active";
    String cssItemImage = ".image-with-description";

    open("/sklep");
    $$(cssItemImage).first().click();

    assertThat("Opened page url is not as expected", url(), is(startsWith(expectedUrl)));
    assertEquals("Page header is not Kaski", expectedTitle, $(cssPageHeader).shouldBe(visible).getText());
    assertEquals("Active element in list is not Kaski", expectedTitle.toUpperCase(), $(cssActiveInTitle).getText());
  }

  @Test
  public void shouldAddHelmetWhenUserSearchedIt() {
    String cssSearchField = "#shop-search-input";
    String cssFoundHelmet = ".row.cloth";
    String cssTitleFoundHelmet = ".title a";
    String cssPaginatorOnListPage = ".pagination";
    String buyButton = ".fs-large";
    String itemInCart = "div.name a";
    String totalCart = ".total";
    String cssCloseEmail = ".right-corner.close-btn";

    open("/ubrania/kask");
    $(cssSearchField).setValue("Droid").pressEnter();

    $(cssPaginatorOnListPage).shouldBe(Condition.disappear);

    assertEquals("Not one helmet was found", 1, $$(cssFoundHelmet).size());

    $(cssTitleFoundHelmet).click();
    $(cssCloseEmail).click();
    $$(buyButton).first().click();
    $$(buyButton).get(1).click();

    $(itemInCart).shouldBe(visible);

    assertEquals("Not one helmet was found", 1 , $$(itemInCart).size());
    assertEquals("Price is not correct", "949,00", $(totalCart).getText());
    assertEquals("Helmet name is not correct", "Kask Caberg Droid", $(itemInCart).getText());
  }
}
