package com.harman.autotest;

public class SystemUtil {

  public static String getDriverAccToSystemType() {
  if (isWindows()) {
    return "src\\resources\\cd_win32\\chromedriver.exe";
  } else if (isUnix()) {
    return "src/resources/cd_linux64/chromedriver";
  } else {
    throw new RuntimeException("Your system type was not detected! Please download and set chromedriver by yourself");
  }
}

  private static String OS = System.getProperty("os.name").toLowerCase();

  private static boolean isWindows() {
    return OS.contains("win");
  }

  private static boolean isUnix() {
    return OS.contains("nux");
  }
}
